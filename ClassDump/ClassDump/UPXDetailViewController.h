//
//  UPXDetailViewController.h
//  ClassDump
//
//  Created by 暢之 堀内 on 12/05/01.
//  Copyright (c) 2012年 株式会社ユニプロ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UPXDetailViewController : UIViewController <UISplitViewControllerDelegate>

@property (strong, nonatomic) id detailItem;

@end
