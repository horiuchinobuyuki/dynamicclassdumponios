//
//  UPXClass.h
//  ClassDump
//
//  Created by 暢之 堀内 on 12/05/01.
//  Copyright (c) 2012年 株式会社ユニプロ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/objc.h>
#import <objc/runtime.h> 
#import <objc/message.h>

@interface UPXClass : NSObject {
    Class _class;
}

-(id)initWithClassName:(NSString*)className;
-(id)initWithClass:(Class)class;


+(NSArray*)classNames;
+(NSString*)typeStringForType:(char *)type;
+(NSString*)methodDefinitionForProperty:(Method)method;
+(NSString*)propertyDefinitionForProperty:(objc_property_t)property;
+(NSString*)headerForClass:(Class)cls;
+(NSString*)headerForClass:(Class)cls categoryName:(NSString *)category;

-(NSString*)header;

@end
