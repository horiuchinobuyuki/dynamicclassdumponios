//
//  UPXMasterViewController.h
//  ClassDump
//
//  Created by 暢之 堀内 on 12/05/01.
//  Copyright (c) 2012年 株式会社ユニプロ. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UPXDetailViewController;

@interface UPXMasterViewController : UITableViewController

@property (strong, nonatomic) UPXDetailViewController *detailViewController;

@end
