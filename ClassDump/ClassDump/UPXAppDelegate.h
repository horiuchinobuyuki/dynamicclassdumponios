//
//  UPXAppDelegate.h
//  ClassDump
//
//  Created by 暢之 堀内 on 12/05/01.
//  Copyright (c) 2012年 株式会社ユニプロ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UPXAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UINavigationController *navigationController;

@property (strong, nonatomic) UISplitViewController *splitViewController;

@end
